#!/usr/bin/env python3

import os

from error_cats import FlaskErrorCats
from flask import Blueprint, Flask

cats = Blueprint('cats', __name__)
dogs = Blueprint('dogs', __name__)
FlaskErrorCats(cats, status_codes=set(range(0, 600)), animal='cat')
FlaskErrorCats(dogs, status_codes=set(range(0, 600)), animal='dog')


@cats.route('/<int:status_code>')
@dogs.route('/<int:status_code>')
def status_code_view(status_code):
    return '', status_code


app = Flask(__name__)
app.register_blueprint(cats)
app.register_blueprint(cats, url_prefix='/cat')
app.register_blueprint(dogs, url_prefix='/dog')

if __name__ == '__main__':
    port = 5000
    if 'PORT' in os.environ:
        port = os.environ['PORT']
    app.run(host="0.0.0.0", port=port)
