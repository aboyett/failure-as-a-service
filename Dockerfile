FROM alpine:3.9

RUN apk add --no-cache py3-pip

COPY requirements.txt main.py /app/

RUN adduser -D app && chown -R app /app

WORKDIR /app

RUN pip3 install -r requirements.txt

USER app

CMD ["./main.py"]

EXPOSE 5000

